﻿using AutoMapper;
using DAL.ModelsDTO.WebAPI;
using DAL.Entities;

namespace DAL.MappingProfiles
{
    public sealed class TeamsProfile : Profile
    {
        public TeamsProfile()
        {
            CreateMap<Teams, TeamDTO>();
            CreateMap<UpdateTeamDTO, Teams>();
            CreateMap<CreateTeamDTO, Teams> ();
        }
    }
}
