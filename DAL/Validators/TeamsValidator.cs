﻿using DAL.Entities;
using FluentValidation;
using System;

namespace DAL.Validators
{
    public class TeamsValidator : AbstractValidator<Teams>
    {
        public TeamsValidator()
        {
            RuleFor(t => t.Name).NotEmpty().WithMessage("*Required");
            RuleFor(t => t.CreatedAt).NotEmpty().WithMessage("*Required")
                                     .LessThan(DateTime.Now)
                                     .WithMessage("CreatedAt cannot be in future");
            RuleFor(t => t.Efficiency).LessThanOrEqualTo(100).WithMessage("Efficiency cannot be greater 100%");
        }
    }
}