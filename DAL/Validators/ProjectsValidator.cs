﻿using DAL.Entities;
using FluentValidation; 

namespace DAL.Validators
{
    public class ProjectsValidator : AbstractValidator<Projects>
    {
        public ProjectsValidator()
        {
            RuleFor(x => x.CreatedAt).NotEmpty().WithMessage("*Required");
            RuleFor(x => x.Deadline).NotEmpty().WithMessage("Please specify deadline"); 
            RuleFor(x => x.Description).NotEmpty().WithMessage("Description cannot be empty")
                                       .Length(5, 500).WithMessage("Description length is incorrect"); 
        }
    }
}