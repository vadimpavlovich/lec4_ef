﻿using DAL.Entities;
using FluentValidation;
using System;

namespace DAL.Validators
{
    public class UsersValidator : AbstractValidator<Users>
    {
        public UsersValidator()
        {
            RuleFor(u => u.Email).NotEmpty().WithMessage("Email address is required")
                                 .EmailAddress().WithMessage("A valid email is required");

            RuleFor(x => x.FirstName).NotEmpty().WithMessage("FirstName cannot be empty")
                                     .Length(1, 40).WithMessage("FirstName length is incorrect");
            RuleFor(x => x.LastName).NotEmpty().WithMessage("LastName cannot be empty")
                                   .Length(1, 40).WithMessage("LastName length is incorrect");
            RuleFor(x => x.BirthDay).NotEmpty().WithMessage("*Required")
                                    .LessThan(DateTime.Now) 
                                    .WithMessage("BirthDay cannot be in future"); 
        }
    }
}