﻿using System;

namespace DAL.ModelsDTO.WebAPI
{
    public class UpdateTeamDTO
    { 
        public string Name { get; set; } 
        public DateTime CreatedAt { get; set; } 
    }
}
