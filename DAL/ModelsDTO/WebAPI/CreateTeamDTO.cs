﻿using System;

namespace DAL.ModelsDTO.WebAPI
{
    public class CreateTeamDTO
    { 
        public string Name { get; set; } 
        public DateTime CreatedAt { get; set; } 
    }
}
